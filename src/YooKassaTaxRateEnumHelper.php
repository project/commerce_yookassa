<?php

namespace Drupal\yookassa;

use Drupal\Core\StringTranslation\StringTranslationTrait;

class YooKassaTaxRateEnumHelper
{
    use StringTranslationTrait;

    const WITHOUT_VAT = 1;
    const VAT_0 = 2;
    const VAT_10 = 3;
    const VAT_20 = 4;
    const VAT_110 = 5;
    const VAT_120 = 6;
    const VAT_5 = 7;
    const VAT_7 = 8;
    const VAT_105 = 9;
    const VAT_107 = 10;

    public function getTaxRate(): array
    {
        return array(
            self::WITHOUT_VAT => $this->t('Without VAT'),
            self::VAT_0 => $this->t('0%'),
            self::VAT_5 => $this->t('5%'),
            self::VAT_7 => $this->t('7%'),
            self::VAT_10 => $this->t('10%'),
            self::VAT_20 => $this->t('20%'),
            self::VAT_105 => $this->t('Applicable rate 5/105'),
            self::VAT_107 => $this->t('Applicable rate 7/107'),
            self::VAT_110 => $this->t('Applicable rate 10/110'),
            self::VAT_120 => $this->t('Applicable rate 20/120'),
        );
    }
}
