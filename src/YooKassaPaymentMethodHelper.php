<?php

namespace Drupal\yookassa;

use Drupal;
use Drupal\commerce_payment\Entity\PaymentGateway;
use Drupal\Core\Entity\EntityStorageException;
use Exception;

class YooKassaPaymentMethodHelper
{
    /**
     * Проверка на существование записи платежного шлюза
     *
     * @param string $machineName
     * @return bool
     */
    public static function check(string $machineName): bool
    {
        $paymentMethod = Drupal::configFactory()->getEditable('commerce_payment.commerce_payment_gateway.' . $machineName)->getOriginal('configuration');
        return (bool)$paymentMethod;
    }

    /**
     * Подготовка массива для создания нового платежного шлюза
     *
     * @param array $formData
     * @param string $machineName
     * @return array
     */
    public static function prepareArray(array $formData, string $machineName): array
    {
        $result = [
            'id' => $machineName,
            'label' => $formData['label'],
            'plugin' => 'yookassa',
            'status' => 0,
            'dependencies' => [
                'module' => 'yookassa'
            ]
        ];

        $result['configuration']['display_label'] = $formData['configuration']['yookassa']['display_label'];

        foreach ($formData['configuration']['yookassa']['column'] as $name => $value) {
            $result['configuration'][$name] = $value;
        }

        return $result;
    }

    /**
     * Подготовка данных и сохранение нового платежного шлюза
     *
     * @param string $formData
     * @param string $machineName
     * @throws EntityStorageException
     * @throws Exception
     */
    public static function savePaymentMethod(string $formData, string $machineName)
    {
        if (empty($formData)) {
            throw new Exception('Form data is empty');
        }
        $data = self::parsingFormData($formData);

        if (empty($data)) {
            throw new Exception('Form data parsing error');
        }
        $paymentArray = self::prepareArray($data, $machineName);
        self::saveMethod($paymentArray);
    }

    /**
     * Парсинг данных формы
     *
     * @param string $formData
     * @return array
     */
    public static function parsingFormData(string $formData): array
    {
        $result = urldecode($formData);
        parse_str($result, $form);
        return $form;
    }

    /**
     * Сохранение в БД нового платежного шлюза
     *
     * @param array $paymentArray
     * @return void
     * @throws EntityStorageException
     */
    public static function saveMethod(array $paymentArray)
    {
        $payment_gateway = PaymentGateway::create($paymentArray);
        $payment_gateway->setPluginConfiguration($paymentArray['configuration']);
        $payment_gateway->save();
    }
}
