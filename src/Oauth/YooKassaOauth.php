<?php

namespace Drupal\yookassa\Oauth;

use Drupal;
use Drupal\Core\Config\Config;
use Exception;

class YooKassaOauth
{
    const OAUTH_CMS_URL = 'https://yookassa.ru/integration/oauth-cms';

    const AUTHORIZATION = 'authorization';
    const GET_TOKEN = 'get-token';
    const REVOKE_TOKEN = 'revoke-token';

    const CMS_NAME = 'drupal';

    /**
     * Конфигурация платежного шлюза, которую можно отредактировать и сохранить в БД
     * @var Config
     */
    public $paymentMethodEditConfig;

    /**
     * Конфигурация платежного шлюза преобразованная в массив
     * @var array
     */
    public $arrayConfig;

    /**
     * Уникальный id для запросов в OAuth приложение
     * @var string
     */
    public $state;

    /**
     * @param string|null $machineName - машинное имя платежного шлюза
     */
    public function __construct(string $machineName = null)
    {
        $this->paymentMethodEditConfig = Drupal::configFactory()->getEditable('commerce_payment.commerce_payment_gateway.' . $machineName);
        $this->arrayConfig = $this->paymentMethodEditConfig->getOriginal('configuration');
        $this->state = $this->getOauthState();
    }

    /**
     * Формирование данных и отправка запроса на получение ссылки для авторизации
     *
     * @return array
     * @throws Exception
     */
    public function generateOauthUrl(): array
    {
        $parameters = array(
            'state' => $this->state,
            'cms' => self::CMS_NAME,
            'host' => $_SERVER['HTTP_HOST']
        );

        $this->log('Sending request for OAuth link. Request parameters: ' . json_encode($parameters));

        return $this->sendRequest(self::AUTHORIZATION, $parameters);
    }

    /**
     * Формирование данных и отправка запроса на получение токена авторизации
     *
     * @return array
     * @throws Exception
     */
    public function generateOauthToken(): array
    {
        $parameters = array('state' => $this->state);

        $this->log('Sending request for OAuth token. Request parameters: ' . json_encode($parameters));

        return $this->sendRequest(self::GET_TOKEN, $parameters);
    }

    /**
     * Проверяет в БД state и возвращает его, если нет в БД, генерирует его
     *
     * @return string state - уникальный id для запросов в OAuth приложение
     */
    public function getOauthState(): string
    {
        $state = !empty($this->arrayConfig['oauth_state']) ? $this->arrayConfig['oauth_state'] : null;

        if (!$state) {
            $state = substr(md5(time()), 0, 12);
            $this->saveConfigurationPayment(['configuration.oauth_state' => $state]);
        }

        return $state;
    }

    /**
     * Выполняет запрос в OAuth приложение на отзыв токена
     *
     * @return void
     * @throws Exception
     */
    public function revokeOldToken(string $token)
    {
        $parameters = array(
            'state' => $this->state,
            'token' => $token,
            'cms' => self::CMS_NAME
        );

        $this->log('Sending request to revoke OAuth token. Request parameters: ' . json_encode($parameters));

        $result = $this->sendRequest(self::REVOKE_TOKEN, $parameters);

        $body = json_decode($result['response'], true);

        if (!isset($body['success'])) {
            $this->log(
                'Got error while revoking OAuth token. Response body: '
                . json_encode($body), 'error'
            );
        }
    }

    /**
     * Отправка POST запроса и получение данных
     *
     * @param string $url
     * @param array $parameters
     * @return array
     * @throws Exception
     */
    private static function sendRequest(string $url, array $parameters): array
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::OAUTH_CMS_URL . '/' . $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type' => 'application/json; charset=utf-8'));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($parameters));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        $response = curl_exec($ch);
        $info = curl_getinfo($ch);

        if ($response === false) {
            throw new Exception('Exception: ' . curl_errno($ch) . ": " . curl_error($ch));
        }

        return array('code' => $info['http_code'], 'response' => $response);
    }

    /**
     * Сохраняет информацию о магазине
     *
     * @return void
     * @throws Exception
     */
    public function saveShopInfoByOauth()
    {
        $apiClient = YooKassaClientFactory::getYooKassaClient($this->arrayConfig);
        $shopInfo = $apiClient->me();

        if (!isset($shopInfo['account_id'])) {
            throw new Exception('Failed to save shop info');
        }

        $this->saveConfigurationPayment([
            'configuration.shop_id' => $shopInfo['account_id'],
        ]);
    }

    /**
     * @param string $message
     * @param string $type
     */
    private static function log(string $message, string $type = 'info')
    {
        Drupal::logger('yookassa')->$type($message);
    }

    /**
     * Запись полей конфигурации платежного шлюза
     *
     * @param array $configs
     * @return void
     */
    public function saveConfigurationPayment(array $configs)
    {
        foreach ($configs as $key => $value) {
            $this->paymentMethodEditConfig->set($key, $value);
        }
        $this->paymentMethodEditConfig->save(true);
    }
}
