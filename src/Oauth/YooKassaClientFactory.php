<?php

namespace Drupal\yookassa\Oauth;

use Drupal;
use Drupal\Core\Config\Config;
use Drupal\yookassa\Plugin\Commerce\PaymentGateway\YooKassa;
use YooKassa\Client;

/**
 * Фабрика для получения единственного статического экземпляра клиента API Юkassa
 */
class YooKassaClientFactory
{
    const YOOKASSA_SHOP_ID = 'shop_id';
    const YOOKASSA_SHOP_PASSWORD = 'secret_key';
    const YOOKASSA_ACCESS_TOKEN_KEY = 'access_token';

    const CMS_NAME = 'drupal';
    const MODULE_NAME = 'yoo_api_drupal8';

    /**
     * @var Client
     */
    private static $client;

    /**
     * Возвращает единственный инстанс клиента API Юkassa
     *
     * @param array $config
     * @return Client
     */
    public static function getYooKassaClient(array $config): Client
    {
        if (!self::$client) {
            self::$client = self::getClient($config);
        }

        return self::$client;
    }

    /**
     * Возвращает объект клиента API Юkassa
     *
     * @param array $config
     * @return Client
     */
    private static function getClient(array $config): Client
    {
        $apiClient = new Client();

        $oauthToken = !empty($config[self::YOOKASSA_ACCESS_TOKEN_KEY]) ? $config[self::YOOKASSA_ACCESS_TOKEN_KEY] : null;

        if ($oauthToken) {
            $apiClient->setAuthToken($oauthToken);
            self::setApiClientData($apiClient);

            return $apiClient;
        }

        $shopId = $config[self::YOOKASSA_SHOP_ID];
        $password = $config[self::YOOKASSA_SHOP_PASSWORD];

        $apiClient->setAuth($shopId, $password);

        self::setApiClientData($apiClient);

        return $apiClient;
    }

    /**
     * Устанавливает значение свойств для user-agent
     *
     * @param $apiClient
     * @return void
     */
    private static function setApiClientData($apiClient)
    {
        $userAgent = $apiClient->getApiClient()->getUserAgent();
        $userAgent->setCms(self::CMS_NAME, Drupal::VERSION);
        $userAgent->setModule(self::MODULE_NAME, YooKassa::YOOMONEY_MODULE_VERSION);
    }
}
