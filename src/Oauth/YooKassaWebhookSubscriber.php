<?php

namespace Drupal\yookassa\Oauth;

use Drupal\Core\Config\Config;
use YooKassa\Client;
use YooKassa\Model\NotificationEventType;

class YooKassaWebhookSubscriber
{
    /**
     * Проверяет существующие подписки, удаляет некорректные и создает новые
     *
     * @param Client $client
     * @param array $config
     * @return void
     */
    public static function subscribe(Client $client, array $config)
    {
        $needWebHookList = array(
            NotificationEventType::PAYMENT_SUCCEEDED,
            NotificationEventType::PAYMENT_CANCELED,
            NotificationEventType::PAYMENT_WAITING_FOR_CAPTURE,
            NotificationEventType::REFUND_SUCCEEDED,
        );

        $webHookUrl = $config['notification_url'];

        $currentWebHookList = $client->getWebhooks()->getItems();
        foreach ($needWebHookList as $event) {
            $hookIsSet = false;
            foreach ($currentWebHookList as $webHook) {
                if ($webHook->getEvent() === $event) {
                    if ($webHook->getUrl() === $webHookUrl) {
                        $hookIsSet = true;
                        continue;
                    }

                    $client->removeWebhook($webHook->getId());
                }
            }
            if (!$hookIsSet) {
                $client->addWebhook(array('event' => $event, 'url' => $webHookUrl));
            }
        }
    }
}
