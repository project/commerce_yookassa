<?php

namespace Drupal\yookassa\Plugin\Commerce\PaymentGateway;


use Drupal;
use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\Payment;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_tax\Entity\TaxType;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Config\Config;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\GeneratedUrl;
use Drupal\yookassa\Oauth\YooKassaClientFactory;
use Drupal\yookassa\YooKassaTaxRateEnumHelper;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use YooKassa\Client;
use YooKassa\Common\Exceptions\ApiException;
use YooKassa\Common\Exceptions\BadApiRequestException;
use YooKassa\Common\Exceptions\ExtensionNotFoundException;
use YooKassa\Common\Exceptions\ForbiddenException;
use YooKassa\Common\Exceptions\InternalServerError;
use YooKassa\Common\Exceptions\NotFoundException;
use YooKassa\Common\Exceptions\ResponseProcessingException;
use YooKassa\Common\Exceptions\TooManyRequestsException;
use YooKassa\Common\Exceptions\UnauthorizedException;
use YooKassa\Model\Notification\NotificationFactory;
use YooKassa\Model\PaymentStatus;
use YooKassa\Request\Payments\Payment\CreateCaptureRequest;
use YooKassa\Request\Refunds\RefundResponse;

/**
 *
 * @CommercePaymentGateway(
 *   id = "yookassa",
 *   label = "YooKassa",
 *   display_label = "YooKassa",
 *   forms = {
 *     "offsite-payment" = "Drupal\yookassa\PluginForm\YooKassa\PaymentOffsiteForm",
 *     "test-action" = "Drupal\yookassa\PluginForm\YooKassa\PaymentMethodAddForm"
 *   },
 *   payment_method_types = {
 *     "yookassa_epl"
 *   },
 *   modes = {
 *     "n/a" = @Translation("N/A"),
 *   }
 * )
 *
 */
class YooKassa extends OffsitePaymentGatewayBase
{
    const YOOMONEY_MODULE_VERSION = '2.3.5';

    const CMS_NAME = 'yoo_api_drupal8';

    /**
     * @var Client apiClient
     */
    public $apiClient;

    /**
     * Конфигурация платежного шлюза
     * @var array
     */
    public $config = [];

    /**
     * Отсортированный список налогов
     * @var array
     */
    public $taxes = [];

    /**
     * {@inheritdoc}
     */
    public function __construct(
        array  $configuration,
        string $plugin_id,
        array  $plugin_definition,
        EntityTypeManagerInterface $entity_type_manager,
        PaymentTypeManager $payment_type_manager,
        PaymentMethodTypeManager $payment_method_type_manager,
        TimeInterface $time
    ) {
        parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager,
            $payment_method_type_manager, $time);
        $machineName = Drupal::routeMatch()->getParameters()->get('commerce_payment_gateway');
        $this->config = !empty($machineName) ? $this->getPaymentMethodConfig($machineName->id()) : null;
        $this->apiClient = !empty($this->config) ? YooKassaClientFactory::getYooKassaClient($this->config) : null;
        $this->taxes = (new YooKassaTaxRateEnumHelper())->getTaxRate();
    }

    /**
     * {@inheritdoc}
     */
    public function defaultConfiguration(): array
    {
        return [
                'shop_id' => '',
                'secret_key' => '',
                'description_template' => '',
                'receipt_enabled' => '',
                'default_tax' => '',
                'yookassa_tax' => [],
                'notification_url' => '',
                'second_receipt_enabled' => '',
                'order_type' => [],
                'second_receipt_status' => [],
                'default_tax_rate' => [],
                'default_payment_subject' => [],
                'default_payment_mode' => [],
                'oauth_state' => '',
                'token_expires_in' => '',
                'access_token' => '',
            ] + parent::defaultConfiguration();
    }

    /**
     * Построение формы настроек модуля
     *
     * @param array $form
     * @param FormStateInterface $form_state
     * @return array
     * @throws InvalidPluginDefinitionException|PluginNotFoundException
     */
    public function buildConfigurationForm(array $form, FormStateInterface $form_state): array
    {
        $shopInfo = $this->getShopInfo();
        $values = $form_state->getValue($form['#parents']);
        $form['column'] = [
            '#type' => 'container',
            '#attributes' => ['id' => 'columns-wrapper'],
        ];

        $form['column']['oauth_setting'] = array(
            '#markup' => '<h5 class="qa-title">' . $this->t('Link your website on Drupal to the YooMoney Merchant Profile') . '</h5>',
        );

        $this->createAuthorizationFields($form, $shopInfo);

        $form = parent::buildConfigurationForm($form, $form_state);

        $form['column']['description_template'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Payment description'),
            '#description' => $this->t('This is the description of the transaction: customers will see it when making the payment, and you\'ll see it in the Merchant Profile. For example, «Payment for order No. 72».').'<br>'.
                $this->t('If you\'d like the order ID to be autofilled into the description, put %order_id% in its place (Payment for order No. %order_id%).').'<br>'.
                $this->t('Maximum length for the description is 128 characters.'),
            '#default_value' => !empty($this->configuration['description_template'])
                ? $this->configuration['description_template']
                : $this->t('Payment for order No. %order_id%'),
        ];

        $receiptEnabled = $this->checkValuesField($values, 'receipt_enabled');
        $this->createSendCheckMessage($form, $receiptEnabled, $shopInfo);

        $form['column']['receipt_enabled'] = [
            '#type' => 'checkbox',
            '#title' => '<span class="qa-enable-receipt-label">' . $this->t('Send data for receipts to YooMoney (54-FZ)') . '</span>',
            '#default_value' => $this->configuration['receipt_enabled'],
            '#ajax' => [
                'callback' => [$this, 'verifyingReceipt'],
                'event' => 'change',
                'progress' => [
                    'type' => 'throbber',
                    'message' => $this->t('Verifying second receipt settings...'),
                ],
                'wrapper' => 'columns-wrapper',
            ],
            '#attributes' => [
                'class' => ['qa-enable-receipt-control'],
            ],
        ];

        $form['column']['check_receipt_enabled_wrapper_begin'] = [
            '#markup' => new FormattableMarkup('<div id="check_receipt_enabled" style="display: @display">', ['@display' => $receiptEnabled ? "block" : "none"]),
        ];

        $form['column']['default_tax'] = [
            '#type' => 'select',
            '#title' => $this->t('Default rate'),
            '#options' => $this->taxes,
            '#default_value' => $this->configuration['default_tax'],
        ];

        $tax_storage = $this->entityTypeManager->getStorage('commerce_tax_type');
        $taxTypes = $tax_storage->loadMultiple();
        $taxRates = [];
        foreach ($taxTypes as $taxType) {
            /** @var TaxType $taxType */
            $taxTypeConfiguration = $taxType->getPluginConfiguration();
            $taxRates += $taxTypeConfiguration['rates'];
        }

        if ($taxRates) {

            $form['column']['yookassa_tax_label'] = [
                '#type' => 'html_tag',
                '#tag' => 'label',
                '#value' => $this->t('Compare the receipts'),
                '#state' => [
                    'visible' => [
                        [
                            [':input[name="measurementmethod"]' => ['value' => '5']],
                            'xor',
                            [':input[name="measurementmethod"]' => ['value' => '6']],
                            'xor',
                            [':input[name="measurementmethod"]' => ['value' => '7']],
                        ],
                    ],
                ],
            ];

            $form['column']['yookassa_tax_wrapper_begin'] = [
                '#markup' => '<div>',
            ];

            $form['column']['yookassa_label_shop_tax'] = [
                '#markup' => new FormattableMarkup('<div style="float: left; width: 200px;">'. $this->t('Rate in your store') . '</div>', []),
            ];

            $form['column']['yookassa_label_tax_rate'] = [
                '#markup' => '<div>'. $this->t('Rate for the receipt for the Tax Service') . '</div>',
            ];

            $form['column']['yookassa_tax_wrapper_end'] = [
                '#markup' => '</div>',
            ];

            foreach ($taxRates as $taxRate) {
                $form['column']['yookassa_tax']['yookassa_tax_label_' . $taxRate['id'] . '_begin'] = [
                    '#markup' => '<div>',
                ];
                $form['column']['yookassa_tax']['yookassa_tax_label_' . $taxRate['id'] . '_lbl'] = [
                    '#markup' => new FormattableMarkup('<div style="width: 200px; float: left;padding-top: 5px;"><label>' . $taxRate['label'] . '</label></div>', []),
                ];

                $defaultTaxValue = $this->configuration['yookassa_tax'][$taxRate['id']] ?? 1;
                $form['column']['yookassa_tax'][$taxRate['id']] = [
                    '#type' => 'select',
                    '#title' => false,
                    '#label' => false,
                    '#options' => $this->taxes,
                    '#default_value' => $defaultTaxValue,
                ];

                $form['column']['yookassa_tax']['yookassa_tax_label_' . $taxRate['id'] . '_end'] = [
                    '#markup' => '</div>',
                ];
            }
        }

        $form['column']['default_tax_rate'] = [
            '#type' => 'select',
            '#title' => $this->t('Default tax system'),
            '#options' => [
                1 => $this->t('General tax system'),
                2 => $this->t('Simplified (STS, income)'),
                3 => $this->t('Simplified (STS, income with costs deducted)'),
                4 => $this->t('Unified tax on imputed income (ENVD)'),
                5 => $this->t('Unified agricultural tax (ESN)'),
                6 => $this->t('Patent Based Tax System'),
            ],
            '#default_value' => $this->configuration['default_tax_rate'],
        ];

        $form['column']['default_tax_rate_wrapper_begin'] = [
            '#markup' => new FormattableMarkup('<div>'. $this->t('Select the default tax system. This parameter is only required if you use multiple tax systems, otherwise it\'s not specified'), []),
        ];

        $form['column']['default_tax_rate_wrapper_end'] = [
            '#markup' => new FormattableMarkup('</div>', []),
        ];

        $form['column']['taxes_wrapper_begin'] = [
            '#markup' => new FormattableMarkup('<div style="width: 50%; display: inline-block;">', []),
        ];

        $form['column']['taxes_block_left_wrapper_begin'] = [
            '#markup' => new FormattableMarkup('<div style="width: 50%; float: left">', []),
        ];

        $form['column']['default_payment_subject'] = [
            '#type' => 'select',
            '#title' => $this->t('Payment subject'),
            '#options' => [
                'commodity' => $this->t('Product (commodity)'),
                'excise' => $this->t('Excisable goods (excise)'),
                'job' => $this->t('Job (job)'),
                'service' => $this->t('Service (service)'),
                'gambling_bet' => $this->t('Gambling bet (gambling_bet)'),
                'gambling_prize' => $this->t('Gambling winnings (gambling_prize)'),
                'lottery' => $this->t('Lottery ticket (lottery)'),
                'lottery_prize' => $this->t('Lottery winnings (lottery_prize)'),
                'intellectual_activity' => $this->t('Intellectual property (intellectual_activity)'),
                'payment' => $this->t('Payment (payment)'),
                'agent_commission' => $this->t('Agent’s commission (agent_commission)'),
                'composite' => $this->t('Several subjects (composite)'),
                'another' => $this->t('Another (another)'),
            ],
            '#default_value' => $this->configuration['default_payment_subject'],
        ];

        $form['column']['taxes_block_left_wrapper_end'] = [
            '#markup' => new FormattableMarkup('</div>', []),
        ];

        $form['column']['taxes_block_right_wrapper_begin'] = [
            '#markup' => new FormattableMarkup('<div style="width: 50%; float: right">', []),
        ];

        $form['column']['default_payment_mode'] = [
            '#type' => 'select',
            '#title' => $this->t('Payment method'),
            '#options' => [
                'full_prepayment' => $this->t('Full prepayment (full_prepayment)'),
                'partial_prepayment' => $this->t('Partial prepayment (partial_prepayment)'),
                'advance' => $this->t('Advance payment (advance)'),
                'full_payment' => $this->t('Full payment (full_payment)'),
                'partial_payment' => $this->t('Partial payment and loan (partial_payment)'),
                'credit' => $this->t('Loan (credit)'),
                'credit_payment' => $this->t('Loan repayment (credit_payment)'),
            ],
            '#default_value' => $this->configuration['default_payment_mode'],
        ];

        $form['column']['taxes_block_right_wrapper_end'] = [
            '#markup' => new FormattableMarkup('</div>', []),
        ];


        $form['column']['taxes_wrapper_end'] = [
            '#markup' => new FormattableMarkup('</div>', []),
        ];


        $form['column']['second_receipt_enabled'] = [
            '#type' => 'checkbox',
            '#title' => '<span class="qa-second-receipt-enable-label">' . $this->t('Send the second receipt') . '</span>',
            '#default_value' => $this->configuration['second_receipt_enabled'] ?? 0,
            '#ajax' => [
                'callback' => [$this, 'verifyingReceipt'],
                'event' => 'change',
                'progress' => [
                    'type' => 'throbber',
                    'message' => $this->t('Verifying second receipt settings...'),
                ],
                'wrapper' => 'columns-wrapper',
            ],
            '#attributes' => [
                'class' => ['qa-second-receipt-enable'],
            ],
        ];

        $secondReceiptEnabled = $this->checkValuesField($values, 'second_receipt_enabled', 'receipt_enabled');

        $form['column']['check_second_receipt_enabled_wrapper_begin'] = [
            '#markup' => new FormattableMarkup('<div id="check_second_receipt_enabled" style="display: @display">', ['@display' => $secondReceiptEnabled ? "block" : "none"]),
        ];

        $form['column']['order_type'] = [
            '#type' => 'select',
            '#title' => $this->t('Order type used on the website'),
            '#label' => false,
            '#options' => $this->getOrderTypes(),
            '#default_value' => $this->configuration['order_type'] ?? [],
            '#empty_option' => $this->t('Select'),
            '#ajax' => [
                'callback' => [$this, 'verifyingOrderStatuses'],
                'event' => 'change',
                'progress' => [
                    'type' => 'throbber',
                    'message' => $this->t('Verifying order status...'),
                ],
                'wrapper' => 'columns-wrapper'
            ],
            '#required' => $secondReceiptEnabled ?? false,
        ];


        $form['column']['second_receipt_status'] = [
            '#type' => 'select',
            '#title' => '<span class="qa-second-receipt-status-label">' . $this->t('Send the second receipt when order status changes to ...') . '</span>',
            '#label' => false,
            '#options' => $this->getStates(!empty($values['column']['order_type']) ? $values['column']['order_type'] : $this->configuration['order_type'] ?? ''),
            '#default_value' => $this->configuration['second_receipt_status'] ?? [],
            '#empty_option' => $this->t('Select'),
            '#required' => $secondReceiptEnabled ?? false,
            '#attributes' => [
                'class' => ['qa-second-receipt-status-select'],
            ],
        ];

        $form['column']['check_second_receipt_enabled_wrapper_end'] = [
            '#markup' => '</div>',
        ];

        $form['column']['check_receipt_enabled_wrapper_end'] = [
            '#markup' => '</div>',
        ];

        if ($form_state->getValue('id') || !empty($this->configuration['notification_url'])) {
            $form['column']['notification_url'] = [
                '#type' => 'textfield',
                '#title' => $this->t('Url for notification'),
                '#default_value' => $this->getPaymentName($form_state),
                '#attributes' => ['readonly' => 'readonly'],
            ];
        }

        $form['column']['log_file'] = [
            '#type' => 'item',
            '#title' => $this->t('Logging'),
            '#markup' => $this->t('View ') .'<a href="' . $GLOBALS['base_url'] . '/admin/reports/dblog?type[]=yookassa"
             target="_blank">'.$this->t('logs').'</a>.'
        ];


        return $form;
    }

    /**
     * Валидация поля секретный ключ
     *
     * @param array $form
     * @param FormStateInterface $form_state
     * @return void
     */
    public function validateConfigurationForm(array &$form, FormStateInterface $form_state)
    {
        $values = $form_state->getValue($form['#parents']);
        if (
            !empty($values['column']['secret_key'])
            && empty($values['column']['access_token'])
            && !preg_match('/^test_.*|live_.*$/i', $values['column']['secret_key'])
        ) {
            $markup = new FormattableMarkup($this->t('Couldn\'t find this secret key. If you\'re sure you copied the key correctly, then it\'s invalid for some reason. Issue and activate a new key'), []);
            $form_state->setError($form['column']['secret_key'], $markup);
        }
    }

    /**
     * Сохранение полей формы
     *
     * {@inheritdoc}
     */
    public function submitConfigurationForm(array &$form, FormStateInterface $form_state)
    {
        parent::submitConfigurationForm($form, $form_state);
        if (!$form_state->getErrors()) {
            $values                                                   = $form_state->getValue($form['#parents']);
            $this->configuration['shop_id']                           = $values['column']['shop_id'] ?? $this->config['shop_id'];
            $this->configuration['secret_key']                        = $values['column']['secret_key'] ?? $this->config['secret_key'];
            $this->configuration['description_template']              = $values['column']['description_template'];
            $this->configuration['receipt_enabled']                   = $values['column']['receipt_enabled'];
            $this->configuration['second_receipt_enabled']            = $values['column']['receipt_enabled'] ? $values['column']['second_receipt_enabled'] : 0;
            $this->configuration['default_tax']                       = $values['column']['default_tax'] ?? [];
            $this->configuration['yookassa_tax']                      = $values['column']['yookassa_tax'] ?? [];
            $this->configuration['second_receipt_status']             = $values['column']['second_receipt_status'] ?? [];
            $this->configuration['order_type']                        = $values['column']['order_type'] ?? 'default';
            $this->configuration['default_tax_rate']                  = $values['column']['default_tax_rate'] ?? [];
            $this->configuration['default_payment_subject']           = $values['column']['default_payment_subject'] ?? [];
            $this->configuration['default_payment_mode']              = $values['column']['default_payment_mode'] ?? [];
            $this->configuration['notification_url']                  = $values['column']['notification_url'] ?? '';
            $this->configuration['access_token']                      = $this->config['access_token'] ?? '';
            $this->configuration['oauth_state']                       = $this->config['oauth_state'] ?? '';
            $this->configuration['token_expires_in']                  = $this->config['token_expires_in']?? '';
        }
    }

    /**
     * Processes the "return" request.
     *
     * @param OrderInterface $order
     *   The order.
     * @param Request $request
     *   The request.
     *
     * @throws NeedsRedirectException
     * @throws InvalidPluginDefinitionException
     * @throws EntityStorageException
     * @throws ApiException
     * @throws BadApiRequestException
     * @throws ForbiddenException
     * @throws InternalServerError
     * @throws NotFoundException
     * @throws ResponseProcessingException
     * @throws TooManyRequestsException
     * @throws UnauthorizedException
     * @throws PluginNotFoundException|ExtensionNotFoundException
     */
    public function onReturn(OrderInterface $order, Request $request)
    {
        $payment_storage = Drupal::entityTypeManager()->getStorage('commerce_payment');
        $payments        = $payment_storage->loadByProperties(['order_id' => $order->id()]);
        if ($payments) {
            $payment = reset($payments);
        }
        /** @var Payment $payment */
        $paymentId           = $payment->getRemoteId();
        $apiClient           = YooKassaClientFactory::getYooKassaClient($this->configuration);
        $cancelUrl           = $this->buildCancelUrl($order);
        $paymentInfoResponse = $apiClient->getPaymentInfo($paymentId);
        $this->log('Payment info: ' . json_encode($paymentInfoResponse));
        if ($paymentInfoResponse->status == PaymentStatus::WAITING_FOR_CAPTURE) {
            $captureRequest = CreateCaptureRequest::builder()->setAmount($paymentInfoResponse->getAmount())->build();
            $paymentInfoResponse  = $apiClient->capturePayment($captureRequest, $paymentId);
            $this->log('Payment info after capture: ' . json_encode($paymentInfoResponse));
        }
        if ($payment->getState()->getString() !== 'new') {
            return;
        }
        if ($paymentInfoResponse->status == PaymentStatus::SUCCEEDED) {
            $payment->setRemoteState($paymentInfoResponse->status);
            $payment->setState('completed');
            $payment->save();
            $this->log('Payment completed');
        } elseif ($paymentInfoResponse->status == PaymentStatus::PENDING && $paymentInfoResponse->getPaid()) {
            $payment->setRemoteState($paymentInfoResponse->status);
            $payment->setState('pending');
            $payment->save();
            $this->log('Payment pending');
        } elseif ($paymentInfoResponse->status == PaymentStatus::CANCELED) {
            $payment->setRemoteState($paymentInfoResponse->status);
            $payment->setState('canceled');
            $payment->save();
            $this->log('Payment canceled');
            throw new NeedsRedirectException($cancelUrl->toString());
        } else {
            $this->log('Wrong payment status: ' . $paymentInfoResponse->status);
            throw new NeedsRedirectException($cancelUrl->toString());
        }
    }

    /**
     * Processes the notification request.
     *
     * @param Request $request
     *   The request.
     *
     * @return Response|null
     *   The response, or NULL to return an empty HTTP 200 response.
     * @throws InvalidPluginDefinitionException
     * @throws EntityStorageException
     * @throws ApiException
     * @throws BadApiRequestException
     * @throws ForbiddenException
     * @throws InternalServerError
     * @throws NotFoundException
     * @throws ResponseProcessingException
     * @throws TooManyRequestsException
     * @throws UnauthorizedException
     * @throws ExtensionNotFoundException|PluginNotFoundException
     */
    public function onNotify(Request $request)
    {
        $rawBody           = $request->getContent();
        $notificationData  = json_decode($rawBody, true);
        if (!$notificationData) {
            return new Response('Bad request', 400);
        }
        $this->log('Notification: ' . $rawBody);
        $notificationModel = (new NotificationFactory())->factory($notificationData);
        $paymentResponse = $notificationModel->getObject();
        if ($paymentResponse instanceof RefundResponse) {
            return new Response('OK', 200);
        }

        $metadata = $paymentResponse->getMetadata();

        if (!empty($metadata['cms_name']) && $metadata['cms_name'] !== self::CMS_NAME) {
            $this->log('This notification not for Drupal 8 - 9. This notification for: ' . $metadata['cms_name']);
            return new Response('OK', 200);
        }
        $paymentResponse   = $notificationModel->getObject();
        $paymentId         = $paymentResponse->id;
        $payment_storage   = Drupal::entityTypeManager()->getStorage('commerce_payment');
        $payments          = $payment_storage->loadByProperties(['remote_id' => $paymentId]);
        if (!$payments) {
            return new Response('Bad request', 400);
        }
        /** @var Payment $payment */
        $payment = reset($payments);
        /** @var Order $order */
        $order = $payment->getOrder();
        $apiClient = YooKassaClientFactory::getYooKassaClient($this->configuration);
        if (!$order) {
            return new Response('Order not found', 404);
        }

        $paymentInfo = $apiClient->getPaymentInfo($paymentId);
        $this->log('Payment info: ' . json_encode($paymentInfo));

        $state = $order->getState()->getValue();
        if ($state !== 'completed') {
            switch ($paymentInfo->status) {
                case PaymentStatus::WAITING_FOR_CAPTURE:
                    $captureRequest  = CreateCaptureRequest::builder()->setAmount($paymentInfo->getAmount())->build();
                    $captureResponse = $apiClient->capturePayment($captureRequest, $paymentId);
                    $this->log('Payment info after capture: ' . json_encode($captureResponse));
                    if ($captureResponse->status == PaymentStatus::SUCCEEDED) {
                        $payment->setRemoteState($paymentInfo->status);
                        $payment->setState('completed');
                        $payment->save();
                        $this->log('Payment completed');

                        return new Response('Payment completed', 200);
                    } elseif ($captureResponse->status == PaymentStatus::CANCELED) {
                        $payment->setRemoteState($paymentInfo->status);
                        $payment->setState('canceled');
                        $payment->save();
                        $this->log('Payment canceled');

                        return new Response('Payment canceled', 200);
                    }
                    break;
                case PaymentStatus::PENDING:
                    $payment->setRemoteState($paymentInfo->status);
                    $payment->setState('pending');
                    $payment->save();
                    $this->log('Payment pending');

                    return new Response(' Payment Required', 402);
                case PaymentStatus::SUCCEEDED:
                    $payment->setRemoteState($paymentInfo->status);
                    $payment->setState('completed');
                    $payment->save();
                    $this->log('Payment complete');

                    return new Response('Payment complete', 200);
                case PaymentStatus::CANCELED:
                    $payment->setRemoteState($paymentInfo->status);
                    $payment->setState('canceled');
                    $payment->save();
                    $this->log('Payment canceled');

                    return new Response('Payment canceled', 200);
            }
        }

        return new Response('OK', 200);
    }

    /**
     * Builds the URL to the "cancel" page.
     *
     * @param OrderInterface $order
     *
     * @return Url The "cancel" page URL.
     * The "cancel" page URL.
     */
    protected function buildCancelUrl(OrderInterface $order): Url
    {
        return Url::fromRoute('commerce_payment.checkout.cancel', [
            'commerce_order' => $order->id(),
            'step'           => 'payment',
        ], ['absolute' => true]);
    }

    /**
     * @param string $message
     */
    private function log(string $message)
    {
        Drupal::logger('yookassa')->info($message);
    }

    /**
     * Формирование массива с доступными типами заказов на сайте
     *
     * @return array
     * @throws InvalidPluginDefinitionException
     * @throws PluginNotFoundException
     */
    private function getOrderTypes(): array
    {
        $entity_type_manager = Drupal::entityTypeManager();
        $order_type_storage = $entity_type_manager->getStorage('commerce_order_type');
        $order_types = $order_type_storage->loadMultiple();

        $result = [];
        foreach ($order_types as $type) {
            $result[$type->id()] = $type->label();
        }

        return $result;
    }

    /**
     * Получение доступных статусов в выбранном типе заказов
     *
     * @param $orderType
     * @return array
     */
    private function getStates($orderType): array
    {
        $result = [];
        $config = !is_array($orderType) ? Drupal::config('commerce_order.commerce_order_type.' . $orderType)->getRawData() : null;

        if (!empty($config)) {
            $workflow_manager = Drupal::service('plugin.manager.workflow');
            $workflow = $workflow_manager->createInstance($config['workflow']);

            foreach ($workflow->getStates() as $state) {
                $result[$state->getId()] = $state->getLabel();
            }
        }

        return $result;
    }

    /**
     * Ajax перестройка формы после внесения изменения в нее пользователем (включение\отключение чекбоксов)
     *
     * @param array $form
     * @return mixed
     */
    public function verifyingReceipt(array $form)
    {
        return $form['configuration']['form']['column'];
    }

    /**
     * Ajax изменение статусов заказов select поле second_receipt_status (после изменения выбора в поле order_type)
     *
     * @param array $form
     * @param FormStateInterface $form_state
     * @return mixed
     */
    public function verifyingOrderStatuses(array &$form, FormStateInterface $form_state)
    {
        $trigger = $form_state->getTriggeringElement();

        $states = $this->getStates($trigger['#value']);
        $form['configuration']['form']['column']['second_receipt_status']['#options'] = $states ?? [];
        return $form['configuration']['form']['column'];
    }

    /**
     * Проверка значения полей для отображения зависимых полей после переформирования формы
     *
     * @param $values
     * @param string $field
     * @param string|null $fieldDepended
     * @return bool
     */
    private function checkValuesField($values, string $field, string $fieldDepended = null): bool
    {
        if (
            !empty($fieldDepended)
            && empty($values['column'][$fieldDepended])
            && empty($this->configuration[$fieldDepended])
        ) {
            return false;
        }

        if (!empty($values['column'][$field]) || !empty($this->configuration[$field])) {
            return true;
        }

        return false;
    }

    /**
     * Формирование url для уведомлений
     * @param string $paymentName
     * @return Url
     */
    private function getNotificationUrl(string $paymentName): Url
    {
        return Url::fromRoute(
            'commerce_payment.notify',
            ['commerce_payment_gateway' => $paymentName],
            ['absolute' => TRUE]
        );
    }

    /**
     * Получение сформированного url для уведомлений
     * @param FormStateInterface $form_state
     * @return GeneratedUrl|string
     */
    private function getPaymentName(FormStateInterface $form_state)
    {
        $name = !empty($form_state->getValue('id')) ? $form_state->getValue('id') : 'yookassa';
        $url = !empty($this->configuration['notification_url']) ? $this->configuration['notification_url'] : $this->getNotificationUrl($name);

        return !is_string($url) ? $url->toString() : $url;
    }

    /**
     * Получение конфигурации платежного шлюза
     *
     * @param string $machineName
     * @return array
     */
    private function getPaymentMethodConfig(string $machineName): array
    {
        return Drupal::config('commerce_payment.commerce_payment_gateway.' . $machineName)->getOriginal('configuration');
    }

    /**
     * Создание полей до и после прохождения oauth авторизации
     *
     * @param array $form
     * @param array|null $shopInfo
     * @return array
     */
    public function createAuthorizationFields(array &$form, array $shopInfo = null): array
    {
        $config = $this->config ?? $this->configuration;
        $form['column']['oauth_wrapper_begin'] = [
            '#markup' => new FormattableMarkup('<div class="oauth_info qa-oauth-info" style="width: 50%; display: flex; justify-content: space-between">', []),
        ];

        $form['column']['oauth_block_left_wrapper_begin'] = [
            '#markup' => new FormattableMarkup('<div>', []),
        ];

        if (!empty($config['access_token'])) {
            $this->createOauthErrorAndFields($form, $config, $shopInfo);
        }

        if (
            !empty($config['shop_id'])
            && !empty($config['secret_key'])
            && empty($config['access_token'])
        ) {
            $this->createShopErrorAndFields($form, $config, $shopInfo);
        }

        if (isset($shopInfo['test']) && $shopInfo['test']) {
            $form['column']['test_shop_text'] = [
                '#markup' => '<div class="field_oauth switch-shop"><strong>' . $this->t('To switch from the test store to the real store, click "Switch store".') . '</strong><br> '
                    . $this->t('In the pop-up window, sign in to your account, give YooMoney access, and select the required store.') . '</div>',
            ];
        }

        $buttonConfig = $this->buttonConfig($config, $shopInfo);

        $form['column']['change_shop'] = [
            '#markup' => new FormattableMarkup('<button class="btn_oauth_connect @class @isNew">@text</button>', [
                '@isNew' => $this->apiClient ? 'old_record' : 'new_record',
                '@text' => $buttonConfig['text'],
                '@class' => $buttonConfig['qa']
            ]),
        ];

        $form['column']['oauth_block_left_wrapper_end'] = [
            '#markup' => new FormattableMarkup('</div>', []),
        ];

        if (
            empty($config['access_token'])
            && !empty($config['secret_key'])
            && $shopInfo
        ) {
            $form['column']['oauth_block_right_wrapper_begin'] = [
                '#markup' => new FormattableMarkup('<div style="margin-left: 10px;">', []),
            ];

            $form['column']['wrapper_text'] = array(
                '#markup' => '<strong>' . $this->t('Where to find the ShopID and secret key') . '</strong>
                    <div>' . $this->t('The sign-in data will be automatically loaded here from the Merchant Profile. Click ') . '<strong>' . $this->t('Change store') . ':</strong></div>
                    <div>' . $this->t('— sign in to YooMoney in the pop-up window,') . '</div><div>' . $this->t('— allow Drupal to access your data') . '</div>',
            );

            $form['column']['oauth_block_right_wrapper_end'] = [
                '#markup' => new FormattableMarkup('</div>', []),
            ];
        }

        $form['column']['oauth_wrapper_end'] = [
            '#markup' => new FormattableMarkup('</div>', []),
        ];

        return $form;
    }

    /**
     * Генерация поля с ошибкой или данных магазина после oauth
     *
     * @param array $form
     * @param array $config
     * @param array|null $shopInfo
     * @return array
     */
    public function createOauthErrorAndFields(array &$form, array $config, array $shopInfo = null): array
    {
        if (!$shopInfo && !empty($config['access_token'])) {
            $this->createError($form, 'oauth_tab');
        }

        if (isset($shopInfo['account_id'], $shopInfo['test'])) {
            $form['column']['payment_method_test'] = [
                '#markup' => new FormattableMarkup('<div class="qa-shop-type" data-qa-shop-type="@data-qa">@test', [
                        '@test' => $shopInfo['test'] ? $this->t('Test store') : $this->t('Real store'),
                        '@data-qa' => $shopInfo['test'] ? 'test' : 'prod'
                    ]) . '</div>',
            ];
            $form['column']['payment_method_shop'] = [
                '#markup' => new FormattableMarkup('<div class="qa-shop-id" data-qa-shop-id="@shopId">Shop ID: @shopId', ['@shopId' => $shopInfo['account_id']]) . '</div>',
            ];
        }

        return $form;
    }

    /**
     * Генерация поля с ошибкой или данных магазина после
     * авторизации по shopId и секретному ключу
     *
     * @param array $form
     * @param array $config
     * @param array|null $shopInfo
     * @return array
     */
    public function createShopErrorAndFields(array &$form, array $config, array $shopInfo = null): array
    {
        if (
            !$shopInfo
            && $config['shop_id']
            && $config['secret_key']
        ) {
            $this->createError($form, 'shop_id');
        }

        if ($shopInfo && empty($config['access_token'])) {
            $form['column']['shop_id'] = [
                '#type' => 'textfield',
                '#title' => $this->t('Shop Id'),
                '#default_value' => $config['shop_id'],
                '#required' => true,
            ];

            $form['column']['secret_key'] = [
                '#type' => 'textfield',
                '#title' => $this->t('Secret Key'),
                '#default_value' => $config['secret_key'],
                '#required' => true,
            ];
        }

        return $form;
    }

    /**
     * Вывод текста и qa селектора на кнопке
     *
     * @param array $config
     * @param array|null $shopInfo
     * @return array
     */
    private function buttonConfig(array $config, array $shopInfo = null): array
    {
        if ($this->isAuthorization($config)) {
            $text = $this->t('Change store');
            $qa = 'qa-change-shop-button';
        }

        if (!$shopInfo && $this->isAuthorization($config)) {
            $text = $this->t('Connect store to YooMoney again');
            $qa = 'qa-yookassa-entrance';
        }

        return [
            'text' => $text ?? $this->t('Connect your store'),
            'qa' => $qa ?? 'qa-connect-shop-button'
        ];
    }

    /**
     * Проверка на подключение и получение информации о магазине
     *
     * @return array|void|null
     */
    private function getShopInfo()
    {
        if (!$this->apiClient) {
            return;
        }

        try {
            $shopInfo = $this->apiClient->me();
        } catch (Exception $e) {
            $this->log('Get shop info error: ' . $e->getMessage());
            return;
        }

        return $shopInfo;
    }

    /**
     * Генерация сообщения об ошибке
     *
     * @param array $form
     * @return array
     */
    private function createError(array &$form): array
    {
        $form['column']['wrapper_text_error'] = array(
            '#markup' => '<div class="error-token"><h5>' .
                $this->t('Couldn\'t link the website to your Merchant Profile') . '</h5> ' .
                $this->t('Connect your store to YooMoney again or ') . '<a href="' . Drupal::request()->getBaseUrl() . '/admin/config/development/performance' . '" target="_blank">' .
                $this->t('clear cache.') . '</a>' .
                $this->t(' If it doesn\'t work, contact tech support.') . '</div>',
        );

        return $form;
    }

    /**
     * Проверка, есть ли данные по одной из авторизаций
     *
     * @param array $config
     * @return bool
     */
    private function isAuthorization(array $config): bool
    {
        return !empty($config['access_token']) || (!empty($config['shop_id']) && !empty($config['secret_key']));
    }

    /**
     * Проверка на включенность фискализации со стороны магазина и модуля
     * и вывод сообщения о необходимости включения, если требуется
     *
     * @param array $form
     * @param bool $receiptEnabled
     * @param array|null $shopInfo
     * @return array|void
     */
    private function createSendCheckMessage(array &$form, bool $receiptEnabled, array $shopInfo = null)
    {
        if (!$shopInfo) {
            return;
        }

        if (
            isset($shopInfo['fiscalization_enabled'])
            && $shopInfo['fiscalization_enabled']
            && !$receiptEnabled
        ) {
            $form['column']['lbl_receipt_enabled_text'] = array(
                '#markup' => '<div><strong>' . $this->t('To activate your online sales register, enable sending data for receipts to YooMoney') . '</strong></div>
            <strong>' . $this->t('Important: ') . '</strong>'. $this->t('if you selected not to link the payment to the receipt, then you don\'t need to enable this feature.'),
            );
        }

        return $form;
    }
}
